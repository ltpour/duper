# DUPER: Disk Usage in Percentages
#
# Copyright 2018 Lasse Pouru
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/bin/sh
shopt -s dotglob
if [ $# -eq 0 ]; then
	dir="./*"; # default to current directory
else
	dir="$1/*";
	if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
		echo "DUPER: Disk usage in percentages";
		echo "Usage: $0 [DIRECTORY]";
		exit;
	fi
fi

names=()
sizes=()
sizes_hr=()
files=-1 # because total = 1

while read -r size name; do
	files=$((files+1))
	names+=("$name")
	sizes+=("$size")
done <<< "$(du -sc --apparent-size $dir 2>/dev/null | sort -hr)"

if [ "$files" -eq "0" ]; then
	echo "Empty directory.";
	exit;
fi

while read -r size name; do
	sizes_hr+=("$size")
done <<< "$(du -sch --apparent-size $dir 2>/dev/null | sort -hr)"

total=${sizes[0]}

i=1
while [ $i -lt $files ]
do
	size=${sizes[i]}
	percent=$(bc <<< "scale=2; $size*100/$total")
	printf "\\033[48;5;7m"
	j=0
	percent_int=$((${percent%.*}))
	while [ $j -le $percent_int ]; do
		printf " ";
		j=$((j+1))
	done
	printf "\\033[0m "
	echo "$percent% (${sizes_hr[i]}) ${names[i]}"
	i=$((i+1))
done
shopt -u dotglob
