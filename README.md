# DUPER #

### Summary ###

DUPER: Disk usage in percentages.

### Usage ###

duper [directory]

### Installation ###

Move duper under /usr/local/bin or another directory in your $PATH.

### Dependencies and requirements ###

- du
- sort

Duper has been tested under GNU/Linux in mksh, but it should work in Bash and other shells as well.

### TODO ###

- limit line length to 80 characters
